const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/rol',async (req,res)=>{
    await conexion.query("select * from rol",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

  router.get('/rol/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("select * from rol where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

  
  router.post('/rol', async (req,res)=>{   
    const {rol,fechaCreacion}=req.body;
    await conexion.query("insert into rol set rol=?,fechaCreacion=?",[rol,fechaCreacion],(err,rows,fields)=>{
       if(!err){
         res.json(rows);
       }else{
          res.json(err);
       }
    })
 })

 router.put('/rol/:id', async (req,res)=>{
  const {id}=req.params;   
  const {rol,fechaCreacion}=req.body;
  await conexion.query("update rol set rol=?,fechaCreacion=? where id=?",[rol,fechaCreacion,id],(err,rows,fields)=>{
     if(!err){
       res.json(rows);
     }else{
        res.json(err);
     }
  })
})

  router.delete('/rol/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("delete from rol where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })


module.exports=router;