const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/fototienda',async (req,res)=>{
    await conexion.query("select * from fototienda",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

  router.get('/fototienda/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("select * from fototienda where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

  
  router.post('/fototienda', async (req,res)=>{   
    const {idTienda,urlImagenes}=req.body;
    await conexion.query("insert into fototienda set idTienda=?,urlImagenes=?",[idTienda,urlImagenes],(err,rows,fields)=>{
       if(!err){
         res.json(rows);
       }else{
          res.json(err);
       }
    })
 })

 router.put('/fototienda/:id', async (req,res)=>{
  const {id}=req.params;   
  const {idTienda,urlImagenes}=req.body[0];
  await conexion.query("update fototienda set idTienda=?,urlImagenes=? where id=?",[idTienda,urlImagenes,id],(err,rows,fields)=>{
     if(!err){
       res.json(rows);
     }else{
        res.json(err);
     }
  })
})

  router.delete('/fototienda/:id',async (req,res)=>{
    const {id}=req.params;
    await conexion.query("delete from fototienda where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })


module.exports=router;