const express=require("express");
const router=express.Router();
const conexion=require('../../conexion/conexion');

router.get('/usuariorol',async (req,res)=>{
    await conexion.query("select * from usuariorol",(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })

  router.get('/usuariorol/:id',(req,res)=>{
    const {id}=req.params;
    conexion.query("select * from usuariorol where id=?",[id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
      }else{
        res.json(err);
      }
    })
  })
  

router.post('/usuariorol', async (req,res)=>{    
    const {idUsuario,idRol}=req.body;
    await conexion.query("insert into usuariorol set idUsuario=?,idRol=?",[idUsuario,idRol],(err,rows,fields)=>{
       if(!err){
         res.json(rows);
       }else{
          res.json(err);
       }
    })
 })
 
router.put('/usuariorol/:id',(req,res)=>{
  const {id}=req.params;
  const {idUsuario,idRol}=req.body[0];
   conexion.query("UPDATE usuariorol SET idUsuario=?,idRol=? WHERE id=?",[idUsuario,idRol,id],(err,rows,fields)=>{
      if(!err){
        res.json(rows);
        console.log(rows);
      }else{
        res.json(err);
      }
  })
})

router.delete('/usuariorol/:id',(req,res)=>{
  const {id}=req.params;
   conexion.query("delete usuario where id=?",[id],(err,rows,fields)=>{
    if(!err){
      res.json(rows);
    }else{
      res.json(err);
    }
  })
})






module.exports=router;