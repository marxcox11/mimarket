const express= require("express");
const cors=require('cors');
const morgan=require('morgan');
const app=express();

//#region configuracion del servidor
origin:'*'
app.use(cors({
}));
app.set('port',process.env.PORT || 3000);

app.use(express.json());
app.use(morgan('dev'));
app.use(express.urlencoded({extend:false}));
//#endregion

//#region rutas de las tablas
app.use(require('./rutas/usuario/rutaUsuario.js'));
app.use(require('./rutas/fototienda/rutaFotoTienda.js'));
app.use(require('./rutas/rol/rutaRol.js'));
app.use(require('./rutas/tienda/rutaTienda.js'));
app.use(require('./rutas/usuariorol/rutaUsuariorol.js'));
app.use(require('./rutas/mensaje/rutaMensaje.js'));
//#endregion

//#region servidor escuchando en el puerto 3000
app.listen(app.get('port'),()=>{
    console.log('servidor',app.get('port'));
})
//#endregion
